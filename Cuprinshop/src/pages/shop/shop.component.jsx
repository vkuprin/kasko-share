import React, { Component } from 'react';
import { convertCollectionsSnapshotToMap, firestore } from '../../firebase/firebase.utils';

import CollectionPage from '../collection/collection.component';
import CollectionsOverview from '../../components/collections-overview/collections-overview.component';
import { Route } from 'react-router-dom';
import WithSpinner from '../../components/with-spinner/with-spinner.component';
import { connect } from 'react-redux';
import { updateCollections } from '../../redux/shop/shop.actions';

const CollectionsOverviewWithSpinner = WithSpinner(CollectionsOverview);
const CollectionPageWithSpinner = WithSpinner(CollectionPage);

class ShopPage extends Component {

	state = {
		loading: true
	}

	unsuscbribeFromSnapshot = null;

	componentDidMount() {
		const { updateCollections } = this.props;

		const collectionRef = firestore.collection('collections');

		collectionRef.onSnapshot(snapshot => {
			const collectionsMap = convertCollectionsSnapshotToMap(snapshot);
			updateCollections(collectionsMap);
			this.setState({ loading: false })
		})
	}

	render() {
		const { match } = this.props;
		const { loading } = this.state;

		return (
			<div className='shop-page'>
				<Route exact path={`${match.path}`} render={(routeProps) => <CollectionsOverviewWithSpinner {...routeProps} isLoading={loading} />} />
				<Route path={`${match.path}/:collectionId`} render={(routeProps) => <CollectionPageWithSpinner {...routeProps} isLoading={loading} />} />
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	updateCollections: collections => dispatch(updateCollections(collections))
})

export default connect(null, mapDispatchToProps)(ShopPage);
