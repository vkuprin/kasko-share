import {
	CustomButtonContainer
} from './custom-button.styles';
import React from 'react';

const CustomButton = ({
	children,
	...otherProps
}) => {
	return ( <
		CustomButtonContainer {
			...otherProps
		} > {
			children
		} < /CustomButtonContainer>
	);
};

export default CustomButton;