import React from 'react';
import {defaultParams, initialGameState} from '../../js/params.js';
import {gameField, panelStyle, messageStyle} from '../../js/styles.js';
import Target from './Target';

let gameParams = {...defaultParams};

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {...initialGameState(), start: false};
    }
    runNewGame = () => {
        this.setState({...initialGameState(), gameover: false, start: true, targetsCnt: 0});
        this.makeGameFlow(this.gameOptions());
    }

    gameOptions = () => ({
        probability: this.state.probability,
        periodMsec: gameParams.periodMsec,
    })

    makeGameFlow = (options) => {
        if (this.state.life === 0) {
            gameParams = {...defaultParams};
            const score = this.state.score;
            this.setState({...initialGameState(), gameover: true, lastscore: this.state.score});
            return false;
        }
        const gameIterator = this.targetGenerate(options);
        setTimeout(
          () => {
              clearInterval(gameIterator);
              options.periodMsec -= gameParams.difficultStepMsec;
              options.probability += gameParams.difficultStepProbability;
              this.makeGameFlow(options, this);
          },
          gameParams.difficultIntervalMsec,
        );
    };

    targetGenerate = ({probability, periodMsec}) => {
        return setInterval(() => {
            if (Math.random() * 100 <= probability) {
                const xpos = Math.random() * (gameParams.fieldWidth - gameParams.targetWidth);
                const ypos = Math.random() * (gameParams.fieldHeight - gameParams.targetHeight);
                this.setState({
                    targets: [
                        ...this.state.targets,
                        <Target
                          id = {this.state.targetsCnt}
                          key = {this.state.targetsCnt}
                          coordinate = {{xpos, ypos}}
                          clickHandler = {this.clickTarget}
                          targetFired = {this.targetFired}
                        />,
                    ],
                    targetsCnt: ++this.state.targetsCnt,
                });
            }
        }, periodMsec);
    };

    clickTarget = (id) => {
        console.log(id);
        const _targets = [...this.state.targets];
        _targets.reduce(
          (acc, curr, i, arr) => {
              if (curr.props.id === id) {
                  arr.splice(i, 1);
                  this.setState({
                      score: ++this.state.score,
                      targets: _targets,
                  });
              }
          },
          _targets[0],
        );
    }

    targetFired = () => {
        const life = this.state.life > 0 ?
          this.state.life - 1 :
          0;
        this.setState({life: life});
    }

    render() {
        const {start, gameover, targets, life, score, lastscore} = this.state;
        return (
          <div>
              <div style={panelStyle}>
                  { gameover !== true &&
                  start === true &&
                  `HP: ${life} Shots: ${score}`
                  }
              </div>
              <div className="game-field" style={gameField}>

                  {
                      start === false &&
                      <div
                        style={messageStyle}
                        onClick={() => this.runNewGame()}
                      >
                          <span className="text-play">Play Game</span>
                      </div>
                  }

                  {
                      gameover === true &&
                      <div style={messageStyle}>
                          <div>Dead targets: {lastscore}</div>
                          <div onClick={() => this.runNewGame()}><span style={{cursor: 'pointer'}}>Try again</span></div>
                      </div>
                  }

                  { targets }

              </div>
          </div>
        );
    }
}

export default Game;
