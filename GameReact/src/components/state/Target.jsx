import React from 'react';
import {defaultParams} from '../../js/params';
import {makeTargetStyle} from '../../js/styles.js';

class Target extends React.Component {
  constructor(props) {
    super(props);
    this.state = {fired: false};
  }

  tooLate = setTimeout(
    () => {
      this.setState({fired: true});
      this.props.targetFired();
    },
    defaultParams.periodForClickMsec,
  );

  clearTimeout = () => {
    clearTimeout(this.tooLate);
    return true;
  };

  render() {
    return (
      <div
        className="text-play"
        onClick={
          () => {
            !this.state.fired &&
            this.clearTimeout() &&
            this.props.clickHandler(this.props.id);
          }
        }
        style={ makeTargetStyle({...this.props.coordinate, fired: this.state.fired}) }
      />
    );
  }
}

export default Target;
