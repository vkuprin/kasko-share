# Game description
## The game is simple - targets will appear on the screen, you need to quickly hit them.

### New targets appear faster and more difficult.

### If you don’t immediately hit the target, three things will happen:

* the target will change color
* will become unbreakable
* the player will be charged
* the game will end when the player uses up all attempts.

## Architecure

### Components

App.js - entry point
```
components / state / Game.js - game logic management

components / state / Target.js - target
```
### Scripts
```
js / params.js - game settings

js / styles.js - styles of game elements

index.css - application styles
```